Orthogonal lattice structure in Haskell
---------------------------------------

This repository contains my failed attempt at implementing parallel orthogonal
lattice structure in Haskell. I'm making my work public in hope that someone
might be able to solve the problems I was unable to solve. See
[this blog post](https://jstolarek.github.io/posts/2014-05-23-parallel-haskell-challange-also-how-to-make-your-research-project-fail.html)
for more information.

The source code is mostly without commentary. This means that you need to
contact me if there is something you don't understand. I will gladly answer any
questions.
